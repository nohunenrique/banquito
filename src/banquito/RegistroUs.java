package banquito;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;






public class RegistroUs {

	private ObjectContainer db = null;
	
	private void abrirRegistro() {
		db = Db4oEmbedded.openFile("Registro usuario");
	}
	private void cerrarRegistro() {
		db.close();
	}
	public void insertarRegistro(Usuario u) {
		abrirRegistro();
		db.store(u);
		cerrarRegistro();
	}
	public Usuario seleccionarPersona(Usuario u) {
		abrirRegistro();
		ObjectSet resultado = db.queryByExample(u);
		Usuario usuario = (Usuario) resultado.next();
		cerrarRegistro();
		return usuario;
	}
	public void actualizarFondo(float fondo, String id) {
		abrirRegistro();
		Usuario u = new Usuario();
		u.setTarjeta(id);
		
		ObjectSet resultado = db.queryByExample(u);
		Usuario preresultado = (Usuario) resultado.next();
		preresultado.setFondos(fondo);
		db.store(preresultado);
		cerrarRegistro();
				
//		abrirRegistro();
//		Persona p = new Persona();
//		p.setId(id);
//		
//		ObjectSet resultado = db.queryByExample(p);
//		Persona preresultado = (Persona) resultado.next();
//		preresultado.setNombreP(nomP);
//		preresultado.setApellidoP(apellP);
//		preresultado.setApellidoM(apellM);
//		preresultado.setEdadP(edad);
//		db.store(preresultado);
//		cerrarRegistro();
		
	}
	
}
