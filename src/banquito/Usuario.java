package banquito;

public class Usuario {
	private int id; 
	private String nombre;
	private String apellP;
	private String apellM;
	private int NIP;
	private String tarjeta;
	private float fondos;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellP() {
		return apellP;
	}
	public void setApellP(String apellP) {
		this.apellP = apellP;
	}
	public String getApellM() {
		return apellM;
	}
	public void setApellM(String apellM) {
		this.apellM = apellM;
	}
	public int getNIP() {
		return NIP;
	}
	public void setNIP(int nIP) {
		NIP = nIP;
	}
	public String getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
	public float getFondos() {
		return fondos;
	}
	public void setFondos(float fondos) {
		this.fondos = fondos;
	}
	@Override
	public String toString() {
		return "Usuario Tarjeta=" + tarjeta +"\nNIP= "+ NIP +"\nFondos=" + fondos;
	}
	
	public float fondo() {
		return fondos;
	}

}
